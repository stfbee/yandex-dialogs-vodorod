package ru.ovm.alisa.vodorod

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.util.matcher.RequestMatcher

@Configuration
class WebSecurityConfig : WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        http.requiresChannel()
            .requestMatchers(RequestMatcher { r -> r.getHeader("X-Forwarded-Proto") != null })
            .requiresSecure()
        http.csrf().disable()
    }
}