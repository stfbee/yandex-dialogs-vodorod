package ru.ovm.alisa.vodorod.controller

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import ru.ovm.alisa.vodorod.data.AliceRequest
import ru.ovm.alisa.vodorod.data.AliceResponse
import ru.ovm.alisa.vodorod.data.Response

@RestController

@RequestMapping("/")
class GreetingController {

    @RequestMapping(
        method = [RequestMethod.POST],
        consumes = [MediaType.APPLICATION_JSON_VALUE],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun yandexHook(@RequestBody request: AliceRequest): AliceResponse = AliceResponse(
        response = Response(
            "H - Водород, О - Кислород, C - Углерод, А ты самый лучший",
            "H - Водород, О - Кислород, C - Углерод, А ты самый лучший",
            emptyArray(),
            true
        ),
        session = request.session
    )

    @GetMapping("/greeting")
    fun greeting(@RequestParam(defaultValue = "vlad") name: String): String = "hello, $name"


}
