package ru.ovm.alisa.vodorod.data

data class NLU(
    val tokens: Array<String>?,
    val entities: Array<Entity>?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as NLU

        if (tokens?.contentEquals(other.tokens?: emptyArray()) != true) return false
        if (entities?.contentEquals(other.entities?: emptyArray()) != true) return false

        return true
    }

    override fun hashCode(): Int {
        var result = tokens?.contentHashCode()!!
        result = 31 * result + entities?.contentHashCode()!!
        return result
    }
}