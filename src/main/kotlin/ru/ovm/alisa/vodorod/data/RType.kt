package ru.ovm.alisa.vodorod.data

enum class RType {
    SimpleUtterance, ButtonPressed
}