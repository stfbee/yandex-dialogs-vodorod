package ru.ovm.alisa.vodorod.data

data class Markup(
    val dangerous_context: Boolean = false
)
