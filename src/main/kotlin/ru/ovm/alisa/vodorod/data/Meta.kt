package ru.ovm.alisa.vodorod.data

data class Meta(
    val locale: String?,
    val timezone: String?,
    val client_id: String?
//    val interfaces: Array<String>?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Meta

        if (locale != other.locale) return false
        if (timezone != other.timezone) return false
        if (client_id != other.client_id) return false
//        if (interfaces?.contentEquals(other.interfaces ?: emptyArray()) != true) return false

        return true
    }

    override fun hashCode(): Int {
        var result = locale.hashCode()
        result = 31 * result + timezone.hashCode()
        result = 31 * result + client_id.hashCode()
//        result = 31 * result + interfaces?.contentHashCode()!!
        return result
    }
}
