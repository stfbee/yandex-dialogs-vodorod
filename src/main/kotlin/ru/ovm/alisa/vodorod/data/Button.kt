package ru.ovm.alisa.vodorod.data

data class Button(
    val hide: Boolean?,
    val payload: Payload?,
    val title: String?,
    val url: String?
)