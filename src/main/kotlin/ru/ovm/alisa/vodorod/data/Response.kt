package ru.ovm.alisa.vodorod.data

data class Response(
    val text: String?,
    val tts: String?,
    val buttons: Array<Button>?,
    val end_session: Boolean?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Response

        if (text != other.text) return false
        if (tts != other.tts) return false
        if (buttons?.contentEquals(other.buttons?: emptyArray()) != true) return false
        if (end_session != other.end_session) return false

        return true
    }

    override fun hashCode(): Int {
        var result = text.hashCode()
        result = 31 * result + tts.hashCode()
        result = 31 * result + buttons?.contentHashCode()!!
        result = 31 * result + end_session.hashCode()
        return result
    }

}
