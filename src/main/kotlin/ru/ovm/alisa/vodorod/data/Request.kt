package ru.ovm.alisa.vodorod.data

data class Request(
    val command: String?,
    val original_utterance: String?,
    val type: RType?,
    val markup: Markup?,
    val payload: Payload?,
    val nlu: NLU?
)
