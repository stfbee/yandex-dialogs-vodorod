package ru.ovm.alisa.vodorod.data

data class EToken(
    val start: Int?,
    val end: Int?
)
