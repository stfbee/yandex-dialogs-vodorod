package ru.ovm.alisa.vodorod.data

data class AliceResponse(
    val response: Response?,
    val session: Session?,
    val version: String = "1.0"
)