package ru.ovm.alisa.vodorod.data

data class AliceRequest(
    val meta: Meta?,
    val request: Request?,
    val session: Session?
)
