package ru.ovm.alisa.vodorod.data

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue
import java.util.*

enum class EType {
    DATETIME,
    FIO,
    GEO,
    NUMBER;

    @JsonValue
    fun toValue(): String? {
        for ((key, value) in namesMap) {
            if (value == this)
                return key
        }

        return null
    }

    companion object {
        private val namesMap = HashMap<String, EType>(3)

        init {
            namesMap["YANDEX.DATETIME"] = DATETIME
            namesMap["YANDEX.FIO"] = FIO
            namesMap["YANDEX.GEO"] = GEO
            namesMap["YANDEX.NUMBER"] = NUMBER
        }

        @JsonCreator
        fun forValue(value: String): EType? {
            return namesMap[value]
        }
    }
}